<?php

$date = '2017-12-31 23:50:50';
$format = 'Y-m-d H:i:s';
$dt = DateTime::createFromFormat($format , $date, new DateTimeZone('Europe/Paris'));

var_dump($dt);
var_dump($dt instanceof DateTime);

echo $dt->format('d/m/Y H:i:s'); // affichage : 31/12/2017 20:50:50
echo "\n";
echo $dt->format('d/m/Y'); // affichage : 31/12/2017
echo "\n";
echo $dt->format('m/d/Y'); // affichage : 12/31/2017
echo "\n";
echo $dt->format('H\\hi'); // affichage : 20h50
echo "\n";
