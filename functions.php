<?php

declare(strict_types=1);

function addition($a, $b) {
  return $a + $b;
}

$result = addition(2, 3);

echo $result;
echo "<br />\n";

// créeez une fonction booleanToString qui :
// - prend une variable en paramètre
// - si la variable est true, renvoie la chaîne de caractères "vrai"
// - si la variable est false, renvoie la chaîne de caractères "faux"

// appelez cette fonction avec :
// - une valeur true et affichez le résultat
// - une valeur false et affichez le résultat

function booleanToString($booleanValue) {
  if ($booleanValue) {
    return "vrai";
  } else {
    return "faux";
  }
}

$result = booleanToString(true);
echo $result;
echo "<br />\n";

$result = booleanToString(false);
echo $result;
echo "<br />\n";

function booleanToString2($booleanValue) {
  return $booleanValue ? "vrai" : "faux";
}

$result = booleanToString2(true);
echo $result;
echo "<br />\n";

$result = booleanToString2(false);
echo $result;
echo "<br />\n";

function booleanToString3($booleanValue) {
  if ($booleanValue === true) {
    return "vrai";
  } else if ($booleanValue === false) {
    return "faux";
  } else {
    return false;
  }
}

$result = booleanToString3("foo bar baz");

if ($result == false) {
  echo "booleanValue n'est pas une valeur booléenne";
  echo "<br />\n";
} else {
  echo $result;
  echo "<br />\n";
}

// @fixme bug type hinting
// définition d'une fonction avec déclaration de type
function booleanToString4(bool $booleanValue) {
  return $booleanValue ? "vrai" : "faux";
}

echo "booléen<br />\n";

echo booleanToString4(true);
echo "<br />\n";

echo booleanToString4(false);
echo "<br />\n";

// @warning ne génère une exception qu'avec le directive strict_types
echo booleanToString4("foo bar baz");
echo "<br />\n";
