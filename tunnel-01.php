<?php

/*
dans tunnel-01.php, créez un formulaire avec :
- un champ texte nommé "product"
- un champ numérique nommé "quantity"
le formulaire doit envoyer les données à tunnel-02.php

dans tunnel-02.php, récupérez les données et insérez-les dans les clés "product" et "quantity" de la variable de session
affichez les clés "product" et "quantity" de la variable de session
ajouter un lien nommé "commander" qui renvoit vers la page tunnel-03.php

dans tunnel-03.php, affichez les clés "product" et "quantity" de la variable de session
*/

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title></title>
  </head>
  <body>

    <form action="tunnel-02.php" method="post">

      <input type="text" name="product" value="" placeholder="produit" /><br />

      <input type="number" name="quantity" value="" /> <label>quantité</label><br />

      <input type="submit" value="valider" />

    </form>

  </body>
</html>
